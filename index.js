/*

//Javascript Synchronous VS Asynchronous

//Synchronous Javascript
//JS by default has a synchronous behavior since one statement is run at a time

console.log("Arigato")
///console.log(Yamite)// This will have an error so the next statement will not run.

for(let i = 0; i <= 100; i++){
	console.log(i);
}

//blocking is a slow process of code

console.log("Gozaimuch")



//Asynchronous Javascript

function printMe(){
	console.log('print me')
}

function test(){
	console.log('test')
}

setTimeout(printMe, 5000)

test()

//The Fetch API allows you to asynchronously request for a resource (data)
//

//fetch() is a method in JS which allows us to send a request to an API and proces its response.
fetch(url, {options})
//url = > the url to resource/routes from the Server
//optional objects = > it contains additional information about our requests such as method, the body, and the headers.





*/

console.log(fetch('https://jsonplaceholder.typicode.com/posts/1'));



//A promise may be in one of 3 possible states: fulfilled, rejected or pending


//Retrieves all posts (GET)
fetch('https://jsonplaceholder.typicode.com/posts',{

method: 'GET'

})
.then(response => response.json())
.then(data => {
	console.log(data)
})

async function fetchData(){

let result = await fetch('https://jsonplaceholder.typicode.com/posts')

//Results returned by fetch
console.log(result);
console.log(typeof result);
console.log(result.body)



let json = await result.json()
console.log(json)

}



fetchData()


//Creating a post
//Create a new post following the Rest API (create, POST)
fetch('https://jsonplaceholder.typicode.com/posts',{

method: "POST",

headers: {

	'Content-Type': 'application/json'

},

body: JSON.stringify({
	title: 'New post',
	body: 'Hello World',
	userID: 1
})

})

.then(response => response.json())
.then(json => console.log(json))


//Updating a post

//update a spehcific post following the Rest API (update, PUT)
//The Put is a method modifying resources where the client sends data that updates the ENTIRE resources. It is used to set an entity's information completely

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: "PUT",

	headers: {
		'Content-Type': 'application/json'
	},

	body: JSON.stringify({
		id: 1,
		title: "Updated post",
		body: "Hello again",
		userId: 1
	})
})

.then(response => response.json())
.then(data => console.log(data))

//Patch method
//Patch method applies a partial update to the resources

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: "PATCH",

	headers: {
		'Content-Type': 'application/json'
	},

	body: JSON.stringify({
		id: 1,
		title: "Updated post",
		body: "Hello again",
		userId: 1
	})
})

.then(response => response.json())
.then(data => console.log(data))

//Delete a post

fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'DELETE'
})

.then(response => response.json())
.then(data => console.log(data))

isActive: false


//Filtering posts
//The data can be filtered by sending the userID along with the URL
// ?
// Syntax:
//Individual Parameters
    //'url?parameterName=value'

//Multiple Parameters
    //'url?parameterName=valueA&paramB=valueB'



 fetch('https://jsonplaceholder.typicode.com/posts?userId=1')

.then(response => response.json())
.then(data => console.log(data))
console.log("hi")

//Retrieving nested/related data
 fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
 .then(response => response.json())
.then(data => console.log(data))